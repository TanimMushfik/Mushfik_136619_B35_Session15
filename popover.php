
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Example of Bootstrap 3 Popover</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover({
                placement : 'top'
            });
        });
    </script>
    <style type="text/css">
        .bs-example{
            margin: 150px 50px;
        }
    </style>
</head>
<body>
<div class="bs-example">
    <button type="button" class="btn btn-primary" data-toggle="popover" title="pondit door 1" data-content="Lets learn HTML">pondit door 1</button>
    <button type="button" class="btn btn-success" data-toggle="popover" title="pondit door 2" data-content="Lets learn CSS">pondit door 2</button>
    <button type="button" class="btn btn-info" data-toggle="popover" title="pondit door 3" data-content="Lets learn bootstrap as it is fun">pondit door 3</button>
    <button type="button" class="btn btn-warning" data-toggle="popover" title="pondit door 4" data-content="Lets learn php">Last popover</button>
</div>
</body>
</html> 